provider "google" {
  credentials = file("./creds/serviceaccount.json")
  project     = "labs-382008"
}