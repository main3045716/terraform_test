resource "google_storage_bucket" "auto-expire" {
  name          = "bucket-${var.name}"
  location      = "US"
  force_destroy = true
}