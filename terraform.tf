terraform {
  backend "gcs" {
    bucket      = "tf-state-716-ci"
    credentials = "./creds/serviceaccount.json"
  }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.65.2"
    }
  }
}