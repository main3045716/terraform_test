# Terraform deployment pipeline

## How it works

Pipeline uses GCP Service Account to authorize and deploy desired resources. 
Required terraform files also have to be provided. 
CI/CD workflow runs on official Hashicorp Terraform image (https://hub.docker.com/r/hashicorp/terraform/) so appropriate runner must be available.

## Variables

To successfully run a pipeline, you need to provide a set of variables.

### Apply

|        |Apply a resource |
|----------------|-------------------------------|
|Name of variable           |`DESTROY`          |
|Value of variable         |`false`            |

### Destroy

|         |Destroy a resource    |Name of resource to destroy|
|---------------------------------------------|--------|--|
|Name of variable                    |`DESTROY`     |`RESOURCE_NAME`|
|Value of variable                    |`true`         |`'google_compute_instance.default'`|


## Global variable (project)

You have to provide variable named **SERVICEACCOUNT** consisting value of JSON formatted GCP Service Account key. 